import {action, computed, makeAutoObservable, observable} from 'mobx';


class mainStore {
    @observable todo: string = '123';
    constructor() {
        makeAutoObservable(this);
    }
    @action addTodo(newTodo: any) {
        this.todo = newTodo;
    }
    @observable userData = {
        name : "",      
        surname : "",       
        email : ""
    };
    @computed get getTodo(): any {
        return this.todo
    }
}
const store = new mainStore();
export default store;