export interface userDataInterface {
    userName: string,
    fullName: string,
    lastLogin: string | Date,
    enable: boolean
}

export interface AppInterface {
    users: userDataInterface[]
}