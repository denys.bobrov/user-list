import axios, { AxiosRequestHeaders } from 'axios';
const MockAdapter = require("axios-mock-adapter");
  // This sets the mock adapter on the default instance
export const initializeAxiosMockAdapter = () => {

    const mock = new MockAdapter(axios, { delayResponse: 2000 });
    let users = [
      {
        userName: 'snc1',
        fullName: 'Deny Bob',
        lastLogin: '2020/01/23, 05:25:00',
        enable: false
    },
    {
        userName: 'mj23',
        fullName: 'Michael JJ',
        lastLogin: '2021/04/12, 12:32:00',
        enable: true
    }
    ]
    // Mock any GET request to /users
    // arguments for reply are (status, data, headers)
    mock.onGet("/users").reply(200, users);
    mock.onPost("/users").reply((data: AxiosRequestHeaders) => {
      users = [...users, JSON.parse(data.data as string)]
      return [200, users]
    }).onGet("/users").reply(200, users)
}
  