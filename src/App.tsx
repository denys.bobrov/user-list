import React from 'react';
import '@progress/kendo-theme-default/dist/all.css';
import './App.css';
import {observer} from 'mobx-react';
import { useRoutes } from 'react-router-dom';
import store from './store';
import { UsersList } from './pages/UsersList';

// eslint-disable-next-line react-hooks/rules-of-hooks
@observer
class App extends React.Component {
  [x: string]: any;
  render() {
    return (
      <div className="App">
        <UsersList />
      </div>
    )
  }
}
export default App;